from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList

# Create your views here.
def show_todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "TodoList": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    TodoListVar = get_object_or_404(TodoList, id=id)
    context = {
        "TodoList_object": TodoListVar,
    }
    return render(request, "Todos/detail.html", context)